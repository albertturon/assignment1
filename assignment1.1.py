
compounds=[]

#Asks for number of compounds that will be used.

compnum = int(input('How many compounds will you use? '))

#Checks if compnum is positive and if it is the program continues

if abs(compnum) == compnum:

    #Asks for the desired volume of the solution
    
    volume = int(input('What volume in mL do you wish to make? '))
    
    #Iterates through each compound
    
    for i in range(compnum):
        
        #Creates a list for each compound where each index represents
        #one variable of the compound.
        
        comp_i=[]
        
        #Asks for the name, the RMM and the concentration of the compound and all
        #of them are added to the list with their correspondent index.
        
        comp_i.append((input('Which compound will you be using? ')))
        comp_i.append((float(input('What is the RMM of {} in grams? '.format(comp_i[0])))))
        comp_i.append((float(input('What concentration should it be? '))))
        print("\n")
        
        #Calculates the ammount of compound used and adds it to the list.
        
        comp_i.append(volume * (comp_i[2]/1000) * comp_i[1])
        
        #Adds the list with the compound info to another list containing 
        #all the other compounds' information
        
        compounds.append(comp_i)
        
        #Prints how much compound you need to add to a certain volume of
        #water to reach the desired concentration
        
        print('You will need to add {} grams of {} to {}ml of water'.format(comp_i[3], comp_i[0], volume))
        print("\n")
        
        #Creates a dictionary whith all the information of the compound and prints it
        
        d_i={"compound": comp_i[0], "RMM(g/mol)": comp_i[1], "concentration (mol/L)": comp_i[2], "Compound used(g)": comp_i[3]}
        print(d_i,"\n")

    
    #Prints a table containing the information of each compound
    #for better comprehension
    #Method for printing a table adapted from code by Ashwini Chaudhary at https://stackoverflow.com/a/17330263
    
    print("{:<15} {:<15} {:<25} {:<25}".format('Compound','RMM(g/mol)','Concentration(mol/L)', 'Compound used(g)'))
    for name, RMM, concentration, compound_used in compounds:
        print("{:<15} {:<15} {:<25} {:<25}".format(name, RMM, concentration, compound_used))
else:
    print("Number of compounds must be a positive whole number")